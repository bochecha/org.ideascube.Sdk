*Note: This is an attempt at re-imagining the way we build and distribute
Ideascube. It is an experiment, and might lead nowhere. Do not rely on anything
here.*

org.ideascube.Sdk
=================

This repository contains the required tools to build the SDK we use to develop
Ideascube applications.

Building the runtimes
---------------------

You will need to install `BuildStream`_ first.

To build the runtimes, simply run::

    $ bst build platform.bst

Using the runtimes
------------------

*Note: You do NOT need to clone this repository in order to use the SDK or
Platform runtimes.*

The actual runtimes are published as `BuildStream`_ build artifacts to be
consumed when building applications.

We currently only support the ``arm`` and ``x86_64`` architectures, and there
is only a ``devel`` version.

.. _BuildStream: https://buildstream.gitlab.io/buildstream/
