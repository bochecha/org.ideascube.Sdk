#!/usr/bin/bash


# Unfortunately, BuildStream doesn't support recursive pipelines yet
#
# https://wiki.gnome.org/Projects/BuildStream/Roadmap/RecursivePipeline
#
# As a result, for now we must export the sdk and platform to a dedicated
# OSTree repository, which we then import into the app.


set -o errexit
set -o nounset
set -o pipefail


DATE=$(date)
REPO=repo-sdk
REMOTE_REPO=mbridon@web1.kymeria.6clones.net:/srv/www/ideascube/repo-sdk

REF_SDK=org.ideascube.Sdk/x86_64/devel
REF_PLATFORM=org.ideascube.Platform/x86_64/devel

rsync -aH --delete-delay ${REMOTE_REPO}/ ${REPO}


# First, the SDK
rm -fr sdk
bst checkout sdk.bst sdk
ostree --repo=${REPO} commit --canonical-permissions --subject="Build on ${DATE}" --branch=${REF_SDK} sdk

# Now, the platform
rm -fr platform
bst checkout platform.bst platform
ostree --repo=${REPO} commit --canonical-permissions --subject="Build on ${DATE}" --branch=${REF_PLATFORM} platform

# Finally, push
ostree --repo=${REPO} summary --update
rsync -aH --delete-delay ${REPO}/ ${REMOTE_REPO}
